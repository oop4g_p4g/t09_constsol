////////////////////////////////////////////////////////////////////////
// OOP Tutorial 9: Constants and initialisation (question 4a)
////////////////////////////////////////////////////////////////////////

//--------include libraries
#include <iostream>	//for cin >>, cout <<
#include <conio.h>	//for kbhit
#include <string>	//for string
using namespace std;


//Question 4a
class Score {
public:
    Score();
    Score( int a);
    Score( const Score& s);
    int getAmount() const;
    void setAmount( int a);
	void show() const;
    //...
private:
    int amount_;
};
Score::Score() {
    cout << "\n Score() called: ";
	amount_ = 0;
}
Score::Score( int a) {
    cout << "\n Score( int a) called: ";
    amount_ = a;
}
Score::Score( const Score& s) {
    cout << "\n Score( const Score&) called: ";
    amount_ = s.amount_;
}
int Score::getAmount() const {
    return amount_;
}
void Score::setAmount( int a) {
    amount_ = a;
}
void Score::show() const {
    cout << amount_;
}

//outside Score class

//Using passed by value parameters 
void i_show( int i) {
   cout << i;
}
void s_show( Score s) {
   cout << s.getAmount();
}
//Using pointers to constant as function parameters 
void pi_show( int* pi) {
   cout << *pi;
}
void cpi_show( const int* cpi) {
   cout << *cpi;
}
void ps_show( Score* ps) {
   cout << ps->getAmount();
}
void cps_show( const Score* cps) {
   cout << cps->getAmount();
}
//Using constant reference variables as function parameters 
void ri_show( int& ri) {
   cout << ri;
}
void cri_cshow( const int& cri) {
   cout << cri;
}
void rs_show( Score& rs) {
   cout << rs.getAmount();
}
void crs_show( const Score& crs) {
   cout << crs.getAmount();
}

Score clone( const Score& crs) {
   return( Score( crs.getAmount()));
}
const Score cclone( const Score& crs) {
   return( Score( crs.getAmount()));
}

int main() { 
//Question 4a & 4b
    
	int x(12);
	Score score( 3); 
	const Score cscore( 5); 

    //passing parameters by value
    cout << "\nShowing 10 with show( int): ";
    i_show( 10);
    cout << "\nShowing x with show( int): ";
    i_show( x);
    cout << "\nShowing score (with Score::show()): ";
    score.show();
    cout << "\nShowing score (with s_show( Score)): ";
    s_show( score);
    cout << "\nShowing cscore (with s_show( Score)): ";
    s_show( cscore);

	//passing parameters through pointers
    cout << "\nShowing score (with ps_show( Score*)): ";
    ps_show( &score);
    cout << "\nShowing score (with cps_show( const Score*)): ";
    cps_show( &score);

//    cout << "\nShowing cscore (with ps_show( Score*)): ";
//    ps_show( &cscore); //error C2664: 'ps_show' : cannot convert parameter 1 from type 'const class Score *'
    cout << "\nShowing cscore (with cps_show( const Score*)): ";
    cps_show( &cscore);

	//passing parameters with references
    cout << "\nShowing score (with rs_show( Score&)): ";
    rs_show( score);
    cout << "\nShowing score (with crs_show( const Score&)): ";
    crs_show( score);

//    cout << "\nShowing cscore (with rs_show( Score&)): ";
//    rs_show( cscore); //error C2664: 'rs_show' : cannot convert parameter 1 from 'const class Score' to 'class Score &'
    cout << "\nShowing cscore (with crs_show( const Score&)): ";
    crs_show( cscore);

	//returning values
    Score s( 12); 
    clone( s).setAmount(1); 
//    cclone( s).setAmount(1);   //error C2662: 'setAmount' : cannot convert 'this' pointer from 'const class Score' to 'class Score &'
	                           //The compiler detects the misuse!

	cout << "\n\nPress any key to quit...";
	while( !_kbhit());
	return 0;
}
