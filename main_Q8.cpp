////////////////////////////////////////////////////////////////////////
// OOP constant Tutorial: More on Classes and Instances (question 8) 
////////////////////////////////////////////////////////////////////////

//--------include libraries 
#include <iostream>	
#include <chrono>
#include <assert.h>
#include <type_traits>
#include <array>
#include <Windows.h>
#include <iomanip>

using namespace std;

//Question 8: const expression

/*
* A stopwatch timer with millisecond resolution 
* Do you have something like this? Do you want to 
* add it to your Utility library?
*/
class Timer
{
    typedef chrono::high_resolution_clock ClockType; //reduce the amount of typing
    ClockType::time_point startPoint;   //remember when we started
public:
    //reset on instantiation
    Timer() : startPoint(ClockType::now())
    {}
    //how long so far?
    double elapsedTimeSecs() const {
        chrono::duration duration = ClockType::now() - startPoint;
        return chrono::duration_cast<chrono::microseconds>(duration).count() * 0.000001;
    }
    //reset the timer
    void reset() {
        startPoint = ClockType::now();
    }
};
 
//traditional function that's a bit slow at runtime
int factorial(int n) {
    return n <= 1 ? 1 : (n * factorial(n - 1));
}

//compile time version of the same thing
constexpr uint64_t factorial2(uint64_t n) {
    //double check this is REALLY calculated at compile time
    //if this ends up executing at runtime then the assert should
    //fire and stop the program
    assert(is_constant_evaluated());  
    return n <= 1 ? 1 : (n * factorial2(n - 1));
}


//if we use a stl array it just means we can run some code
//at initialisation time to fill it with values, so this function
//creates a array and passes it back (which you can't do with a 
//standard array). But these can be thought of as a normal array.
constexpr int TOTAL = 200;
array<uint64_t, TOTAL> fill_array() {
    array<uint64_t, TOTAL> v{0};
    for (int i = 0; i < TOTAL; ++i)
        v[i] = factorial(i);
    return v;
}
int pop = 1;

//same thing, but this should be able to run at compile time
constexpr array<uint64_t, TOTAL> fill_array2() {
    assert(is_constant_evaluated());  //double check this is REALLY calculated at compile time
    array<uint64_t, TOTAL> v{0};
    for (int i = 0; i < TOTAL; ++i)
        v[i] = factorial2(i);
    return v;
}
 


int main() {

    cout << "\nHow long does it take to calculate some consts?\n";

    /*
    * Just call a timewasting function at runtime and then compile time
    */
    cout << "\nA simple function:\n";
    Timer sw;
    uint64_t res = factorial(TOTAL);
    double slowTime = sw.elapsedTimeSecs();
    cout << "\n Run time=" << slowTime << "\n";
    sw.reset();
    constexpr uint64_t res2 = factorial2(TOTAL);
    double fastTime = sw.elapsedTimeSecs();
    cout << "\n Compile time=" << fastTime << "s (" << setprecision(4)  << slowTime/fastTime << " times faster) \n";

    /*
    * Create and fill arrays both at runtime and compile time
    */
    cout << "\nFilling an arry:\n";
    sw.reset();
    const array<uint64_t, TOTAL> arr = fill_array();
    slowTime = sw.elapsedTimeSecs();
    cout << "\n Run time=" << slowTime << "s\n";
    sw.reset();
    constexpr array<uint64_t, TOTAL> arr2 = fill_array2();
    fastTime = sw.elapsedTimeSecs();
    cout << "\n Compile time=" << fastTime << "s (" << setprecision(4) << slowTime / fastTime << " times faster) \n";
     
    cout << "\n\n";
    system("pause");
    return 0;
}
