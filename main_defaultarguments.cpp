////////////////////////////////////////////////////////////////////////
// OOP Tutorial 9: Constants and initialisation (question 6 solution)
////////////////////////////////////////////////////////////////////////

//--------include libraries
#include <iostream>	//for cin >>, cout <<
#include <string>	//for string
using namespace std;

class Score {
	public:
		Score();
		explicit Score( int);
		int getAmount() const;
	private:
		int amount_;
};
Score::Score() 
:amount_( 0)
{}
Score::Score( int a) 
:amount_( a)
{}
int Score::getAmount() const {
	return amount_;
}

//------------class Player
class Player {
public:
	Player( const string& name="PP", const Score& score=Score());
	Player( const Player& player);
	~Player();
	static int getNumberOfPlayers();
	void show() const;
	int getId() const;
	const Score getScore() const;
	const string getName() const;
	void setScoreAmount( int sa);
	void setName( const string& str);
	void setDetails( const string& name="unknown", const Score& score=Score());
private:
	int id_;    
    string name_;
	Score score_;
	static int numberOfPlayers_;
};

int Player::numberOfPlayers_( 0);
int Player::getNumberOfPlayers() {
	return numberOfPlayers_; 
}
Player::Player( const Player& p)
: id_( ++numberOfPlayers_), name_(p.name_), score_( p.score_)
{}
Player::Player( const string& name, const Score& score)
: id_( ++numberOfPlayers_), name_(name), score_( score)
{}
Player::~Player()
{
	--numberOfPlayers_;
}

void Player::show() const {
    cout << "\n \tid_ is " << id_;
    cout << "\n \tname_ is " << name_;
    cout << "\n \tscore_ is " << score_.getAmount();
}

int Player::getId() const {
    return id_;
}
const string Player::getName() const {
    return name_;
}
const Score Player::getScore() const {
    return score_;
}

void Player::setScoreAmount( int amount) {
    cout << "\nScore was: " << score_.getAmount();
	score_ = Score( amount);
    cout << "\nScore is: " << score_.getAmount();
} 
void Player::setName( const string& name) {
    cout << "\nName was: " << name_;
	name_ = name;
    cout << "\nName is: " << name_;
}
void Player::setDetails( const string& name, const Score& score) {
	score_ = score;
	name_ = name;
}


//end class Player
void show( int x=0, const string& str="");



//--------start program
int main()
{

	show(12);
	show(13, "\nvalue is:");

	cout << "\n___Creating Score score...";
	Score score( 10);
	cout << "\n___Showing score's amount: ";
	cout << score.getAmount();
	cout << "\n\nNumber of current players in class Player:: " << Player::getNumberOfPlayers();
	cout << "\n___Creating Player player1( \"fred\", score):";
	Player player1( "fred", score);
	cout << "\n___Showing player1's data:";
	player1.show();
	cout << "\n___Creating Player player2:";
	Player player2;
	cout << "\n___Showing player2's data:";
	player2.show();
	cout << "\n___Creating Player player3(player1):";
	Player player3(player1);
	cout << "\n___Showing player3's data:";
	player3.show();
	cout << "\n\nNumber of current players in class Player:: " << Player::getNumberOfPlayers();

//   cout << "\n___Changing values in player1:";
	//player1.setName( "Ruby");
	//player1.setScore( 50);

	cout << "\n___Changing values in player1:";
	player1.setDetails( "Tony", Score(100));
	player1.show();

	cout << "\n___Changing values in player1:";
	player1.setDetails( "Tony");
	player1.show();

	cout << "\n___Changing values in player1:";
	player1.setDetails();
	player1.show();

	cout << "\n\n";
	system( "pause");
	return 0;
}

void show( int x, const string& str) {
	 cout << str << x;
}