////////////////////////////////////////////////////////////////////////
// OOP Tutorial 9: More on Classes and Instances (question 6)
////////////////////////////////////////////////////////////////////////

//--------include libraries
#include <iostream>	//for cin >>, cout <<
#include <conio.h>	//for _kbhit
#include <string>	//for string
using namespace std;


//Question 6

class Player {
public:
    Player();
    ~Player();
    static int getNumberOfPlayers(); 
	//...
private:
    string name_;
	int id_;
	static int numberOfPlayers;
};

int Player::numberOfPlayers = 0;

Player::Player()
: name_(""), id_( numberOfPlayers+1)
{	
	++numberOfPlayers;
}
Player::~Player()
{
	--numberOfPlayers;
}
int Player::getNumberOfPlayers()
{
	return numberOfPlayers;
}

int main()
{
//Question 6
	cout << "\n\nTesting static data and function members... ";

	cout << "\nNumber of Players: " << Player::getNumberOfPlayers();
    cout << "\nPlayer player";
	Player player;

	cout << "\nNumber of Players: " << Player::getNumberOfPlayers();
	cout << "\n\nPress any key to quit...";
	while( !_kbhit());
	return 0;
}
